@extends('layouts.app')



@section('swipescript')
<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 5,
      spaceBetween: 10,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });

  </script>  
@endsection

@section('content')
    <div class="container">
        <div class="topslider ">
			<div class="slideshow-container">

				<div class="mySlides fade">  					
  					<img src="img/top1.png" style="width:100%"> 					
				</div>

				<div class="mySlides fade"> 					
  					<img src="img/top2.png" style="width:100%"> 					
				</div>

				<div class="mySlides fade"> 					
  					<img src="img/top3.png" style="width:100%"> 					
				</div>

				<div class="mySlides fade"> 					
  					<img src="img/top4.png" style="width:100%"> 					
				</div>
				<div style="text-align:center">
  					<span class="dot"></span> 
  					<span class="dot"></span> 
  					<span class="dot"></span>
				  	<span class="dot"></span>  
					
			</div>
			</div>

		
		</div>		

		<div class="findyour ">
			<div class="imageleft ">
				<img src="img/lap.png" class="laptopimg">
			</div>
			<div class="searchNdetails">
				<h2 class="font-bold font-45 c-black">Find Yours. The best price will talk.</h4>
				<p class=" font-20">Your new shopping assistant specializing in searching for the best price across multiple marketplaces!
			 Psst, you may be able to earn some cashbacks from Octaplus merchants too!</p>
				<p><a href="/" class="learnmore font-20">Learn More >></a></p>
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
  					<div class="collapse navbar-collapse" id="navbarSupportedContent">
   		 				<form class="form-inline my-2 my-lg-0 display-inline">
      						<input class="form-control font-20" type="search" placeholder="Search for deals, etc" aria-label="Search" required="required" size="30">
      						<button class="btn btn-outline-success searchicon " type="submit"><i class="fa fa-search "></i></button>
    					</form>
  					</div>
				</nav>
			</div>	
		</div>

        <div class="cash-checkin ">
            <div class="cashbackstore ">
				<div class="card">
  					<div class="card-header noborder ">
						  <div class="">
							<h5 class="font-30 c-black display-inline">Cashback Stores</h5>
							<a href="/">
								<span class="c-octa display-inline right morestore">More store</span>
							</a>	
						  </div>   					
  					</div>
  					<div class="card-body">
					  	<div class="container">
  							<div class="row row-cols-3 cashbackcol">
    							<div class="col ">
									<a href="/" class="cashbacklink">
										<img src="img/zalora.png" class="cashbackpng">
										<span class="cashbackspan">Up to 2% cashback</span>
									</a>
								</div>
    							<div class="col">
									<a href="/" class="cashbacklink">
										<img src="img/nike.png" class="cashbackpng">
										<span class="cashbackspan">4% Upsized</span>
									</a>
								</div>
    							<div class="col">
									<a href="/" class="cashbacklink">
										<img src="img/motherhood.png" class="cashbackpng">
										<span class="cashbackspan">6% Upsized</span>
									</a>
								</div>
    							<div class="col">
									<a href="/" class="cashbacklink">
										<img src="img/cotton.png" class="cashbackpng">
										<span class="cashbackspan">3% Upsized</span>
									</a>
								</div>
								<div class="col">
									<a href="/" class="cashbacklink">
										<img src="img/althea.png" class="cashbackpng">
										<span class="cashbackspan">5% Upsized</span>
									</a>
								</div>
								<div class="col">
									<a href="/" class="cashbacklink">
										<img src="img/watson.png" class="cashbackpng">
										<span class="cashbackspan">2% Upsized</span>
									</a>
								</div>
  							</div>
						</div>
  					</div>
				</div>
			</div>
            <div class="dailycheck ">
				<div class="card">
  					<div class="card-header noborder">
					  <h5 class="font-30 c-black display-inline">Daily Check-In</h5>
  					</div>
  					<div class="card-body dailycheckcardbody">
					  <div class="container">
  							<div class="row row-cols-2">
    							<div class="col-4 left"><img src="img/reward.png" class="dailycheckpng"></div>
    							<div class="col-8 " >
									<div>
										<p class="font-15 c-black">Earn up to 300 points while you check-in 7 consecutive days</p>
										<button class="bg-octa c-white noneborder font-20" type="submit">Check-In Now</button>
									</div>
								</div>
    							
  							</div>
						</div>
  					</div>
				</div>
			</div>
        </div>
		
		<div class="shopper">
			<div class="card">
  				<div class="card-header">
				  	<div class="">
				  		<h5 class="font-30 c-black display-inline">Shopper Reviews</h5>
						<button class="right bg-octa c-white noneborder font-20" type="submit">View All</button>
					</div> 
				</div> 
  			</div>
 				<div class="card-body noneborder">
				 	<div class="swiper-container">
                    	<div class="swiper-wrapper">
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr1.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile1.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">Wayne Gadget</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Story</p>
    									<p class=" font-12 txt-align-left" >TTRACING SURGE GAMING CHAIR</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Take a seat and be captivated in every moment you plunge right into the virtual reality.
											Buy now at Hoolah through Octaplus will get spcial CNY promotion !!! ">Take a seat and be captivated in every moment you plunge right into the virtual reality.
											Buy now at Hoolah through Octaplus will get spcial CNY promotion !!!
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>	
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr2.jpeg" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile2.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">ezTEC</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Story</p>
    									<p class=" font-12 txt-align-left" >Vivo V20 Review</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Vivo V20 is a gorgeous looking phone. It is slim (~7.3mm) and weighs just right (~171 grams), which is not something we can say for most phones we run into these days. The “Sunset Melody” color, which is predominantly blue, is a tad too flashy for my personal taste, but there are also Midnight Jazz (Black) and Moonlight Sonata color options to choose from. ">
											Vivo V20 is a gorgeous looking phone. It is slim (~7.3mm) and weighs just right (~171 grams), which is not something we can say for most phones we run into these days. The “Sunset Melody” color, which is predominantly blue, is a tad too flashy for my personal taste, but there are also Midnight Jazz (Black) and Moonlight Sonata color options to choose from.
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr3.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile3.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">Octaplus</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Article</p>
    									<p class=" font-12 txt-align-left" >Benefits of Eating Cauliflower</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Is it really that healthy to eat? We turned to science for answer

												Cauliflower would definitely be considered to be the renaissance vegetable—it can do so many things. It can be made into rice, it can become a pizza crust, turn into mash, and even gnocchi. Plus, it can provide nutrients to your diet. Cauliflower is a mild-tasting vegetable that can take on so many different flavors. Basically, there's a lot to love about cauliflower, but it's still important to know about what happens to your body when you eat cauliflower.">
												Is it really that healthy to eat? We turned to science for answer

												Cauliflower would definitely be considered to be the renaissance vegetable—it can do so many things. It can be made into rice, it can become a pizza crust, turn into mash, and even gnocchi. Plus, it can provide nutrients to your diet. Cauliflower is a mild-tasting vegetable that can take on so many different flavors. Basically, there's a lot to love about cauliflower, but it's still important to know about what happens to your body when you eat cauliflower.
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr4.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile3.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">Octaplus</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Article</p>
    									<p class=" font-12 txt-align-left srtitle" >The Best Laptops with an Optical Drive to buy in 2021: HP, Dell, and Lenovo</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="The optical drive is now a niche feature and it seems to have become quite irrelevant, especially when it comes to notebook PCs. In fact, you hardly see any laptop manufacturer launching new products with a CD/DVD drive. But what if you have a large physical collection of movies or maybe handle a lot of data that you need to offload on CD drives every day? Maybe you have a niche use case that still requires a CD every now and then? In that case, there still are a few options on the market that still offer an optical drive. There are numerous old laptops that feature an optical drive, but we have hunted down some of the best and fairly new options so that you don’t have to compromise on any features. Check them out! ">
										The optical drive is now a niche feature and it seems to have become quite irrelevant, especially when it comes to notebook PCs. In fact, you hardly see any laptop manufacturer launching new products with a CD/DVD drive. But what if you have a large physical collection of movies or maybe handle a lot of data that you need to offload on CD drives every day? Maybe you have a niche use case that still requires a CD every now and then? In that case, there still are a few options on the market that still offer an optical drive. There are numerous old laptops that feature an optical drive, but we have hunted down some of the best and fairly new options so that you don’t have to compromise on any features. Check them out!

										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr5.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile3.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">Octaplus</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Article</p>
    									<p class=" font-12 txt-align-left" >MCO,CMCO,RMCO Workout tips at home</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Stuck at home because of COVID-19? Make exercise part of your at-home routine with awesome home workouts. Try these tips to become a pro at working out at home, plus how to stay motivated when no one's watching. ">
											Stuck at home because of COVID-19? Make exercise part of your at-home routine with awesome home workouts. Try these tips to become a pro at working out at home, plus how to stay motivated when no one's watching.
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr6.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile2.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">ezTEC</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Story</p>
    									<p class=" font-12 txt-align-left" >Asus VivoBook S15 Review</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Asus' VivoBook S15 is a good laptop with a uniquely colorful design and innovative ScreenPad 2.0, but disappointing battery life and a dull screen hold it back. With a flashy green-and-orange colorway and innovative secondary screen, the VivoBook S15 is an appealing laptop on the surface. But how does it hold up as a device made for students and young adults? It's a mixed bag. ">
										Asus' VivoBook S15 is a good laptop with a uniquely colorful design and innovative ScreenPad 2.0, but disappointing battery life and a dull screen hold it back. With a flashy green-and-orange colorway and innovative secondary screen, the VivoBook S15 is an appealing laptop on the surface. But how does it hold up as a device made for students and young adults? It's a mixed bag.
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr7.jpeg" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile2.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">ezTEC</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Story</p>
    									<p class=" font-12 txt-align-left" >Xiaomi Mi 10 Pro review</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="The Xiaomi Mi 10 Pro is an accomplished phone, and its screen, cameras and battery capacity are all what you’d expect from a premium device – in particular, the speakers and charging speeds are some of the best you’ll find in a smartphone these days. However, there are a few too many annoying software quirks for the overall experience to be as smooth as we’d like – and the price is quite a bit more than you’d expect for a Xiaomi phone. ">
										The Xiaomi Mi 10 Pro is an accomplished phone, and its screen, cameras and battery capacity are all what you’d expect from a premium device – in particular, the speakers and charging speeds are some of the best you’ll find in a smartphone these days. However, there are a few too many annoying software quirks for the overall experience to be as smooth as we’d like – and the price is quite a bit more than you’d expect for a Xiaomi phone.
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr8.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile4.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">Muhammad Amir Fahmi</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Video</p>
    									<p class=" font-12 txt-align-left" >Simple and Easy</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Simple, easy and beatiful ">
										Simple, easy and beatiful
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr9.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile4.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">AiSha_BRAND</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Video</p>
    									<p class=" font-12 txt-align-left" >Jom singgal di Video saya</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Tiada yang lebih melainkan ciptaanNYA🤔😘🥰🥰 ">
										Tiada yang lebih melainkan ciptaanNYA🤔😘🥰🥰
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
  									<img src="img/sr10.png" class="card-img-top srpng" alt="...">
  									<div class="card-body noneboarder">
    									<img src="img/profile5.png" class="left srprofileicon">
    									<p class="card-text font-15 c-black">PhotoK Malaysia</p>
  									</div>
  									
    									<p class=" c-octa font-10 txt-align-left">Video</p>
    									<p class=" font-12 txt-align-left" >Wonderful of Premium Album</p>
										
    									<p class=" txt-width txt-long txt-align-left srtitle font-10" title="Wonderful 4 type of Premium Album which is #Rabbit, 
										#Panda, #Dog, #Cat that fit 240pcs lomo card Polaroid Photos and business cards 😎🖼️📸
											Best prices at RM2x only
											兔兔、狗狗、熊猫、猫咪相册精美登场～🥰️
											可放240张图片，售价只在RM2x而已。
											PM for order now while stock last. We provide fast delivery  📦  

											Learn more at
											IG: photokmalaysia
											Facebook page : www.facebook.com/photokmalaysia/

											#lomocard #照片 #birthday #album #graduation #photo #valentine #polaroidphoto #anniversary #Love #DIY #情人节 #murah #xmas #christmas #包邮 #present #freeshipping #StringLEDlight #memories #gift #stayathome #fairylight #pospercuma ">
											Wonderful 4 type of Premium Album which is #Rabbit, #Panda, #Dog, #Cat that fit 240pcs lomo card Polaroid Photos and business cards 😎🖼️📸
											Best prices at RM2x only
											兔兔、狗狗、熊猫、猫咪相册精美登场～🥰️
											可放240张图片，售价只在RM2x而已。
											PM for order now while stock last. We provide fast delivery  📦  

											Learn more at
											IG: photokmalaysia
											Facebook page : www.facebook.com/photokmalaysia/

											#lomocard #照片 #birthday #album #graduation #photo #valentine #polaroidphoto #anniversary #Love #DIY #情人节 #murah #xmas #christmas #包邮 #present #freeshipping #StringLEDlight #memories #gift #stayathome #fairylight #pospercuma
										</p>  									
								  		<div class="row ">
    										<div class="col">
     											<img src="https://octaplus.io/img/web/post/default_help.svg" class="btm-icon">
 											</div>
											<div class="col">
												<img src="https://octaplus.io/img/web/post/view.svg" class="btm-icon">
											</div>
											<div class="col">
												<i class="fa fa-bookmark btm-icon"></i>
											</div>
										</div> 									
								</div>
							</div>
                    	</div>    
						</br></br>
                    	<div class="swiper-pagination"></div>
  					</div>	
				</div>
		</div>
       
		
		<div class="item ">
			<div class="card">
  				<div class="card-header">
				  	<div class="">
				  		<h5 class="font-30 c-black display-inline">Item you may like</h5>
						<button class="right bg-octa c-white noneborder font-20" type="submit">View All</button>
					</div> 
				</div> 
  			</div>
 				<div class="card-body">
				 	<div class="swiper-container">
                    	<div class="swiper-wrapper">
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
										
    										</div>										
  										</div>									
										<img src="img/item1.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left">Now you can book a COVID-19 PCR test on Xperience</h6>
    										<p class="font-10 c-black txt-align-left">Travel loka</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											
    										</div>										
  										</div>									
										<img src="img/item2.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left">Malaysia Airlines Special Deals</h6>
    										<p class="font-10 c-black txt-align-left">Travel loka</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											
    										</div>										
  										</div>									
										<img src="img/item3.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6></h6>
    										<p class="font-10 c-black txt-align-left">Travel loka</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											<img src="https://octaplus.io/img/icon/hot.svg" class="item-hot right">
    										</div>										
  										</div>									
										<img src="img/item4.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left">10% off for the first time purchase with Tripcarte Asia</h6>
    										<p class="font-10 c-black txt-align-left">Tripcarte(MY)</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											<img src="https://octaplus.io/img/icon/hot.svg" class="item-hot right">
    										</div>										
  										</div>									
										<img src="img/item5.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left">Purchase Do Not Age Serum D.N.A 12 &enjoy 23% OFF</h6>
    										<p class="font-10 c-black txt-align-left">Cellnique</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											
    										</div>										
  										</div>									
										<img src="img/item6.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left txt-long">VOUCHER LAZADA RM3.00 for RM3.00(CIMB CARD HOLDER ONLY)</h6>
    										<p class="font-10 c-black txt-align-left">Shoppe(MY)</p>
    										<p class="font-10 txt-align-left">No Rating Info (5 sold)</p>
											<p class="font-10 txt-align-left c-octa">MYR4.00</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											
    										</div>										
  										</div>									
										<img src="img/item7.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left txt-long">Lazada Cash Voucer (Chat before order)</h6>
    										<p class="font-10 c-black txt-align-left">Shopee(MY)</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
											<p class="font-10 txt-align-left c-octa">MYR33.70</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											
    										</div>										
  										</div>									
										<img src="img/item8.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left txt-long">Lazada E-Voucher worth RM10</h6>
    										<p class="font-10 c-black txt-align-left">Shopee(MY)</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
											<p class="font-10 txt-align-left c-octa">MYR89.00</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											
    										</div>										
  										</div>									
										<img src="img/item9.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left txt-long txt-width">LAZADA vaucher code(MY) RM10.00 [please message us first before making a purchase]</h6>
    										<p class="font-10 c-black txt-align-left">Shopee(MY)</p>
    										<p class="font-10 txt-align-left">No Rating Info (0 sold)</p>
											<p class="font-10 txt-align-left c-octa">MYR10.00</p>
  										
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col md-8 ">
											 
    										</div>
											<div class="col md-8 ">
											 
    										</div>
    										<div class="col-1 ">
											
    										</div>										
  										</div>									
										<img src="img/item10.png" class="card-img-top itempng" alt="...">
									</a>
  									<div class="card-body">
									  	<div class="card-body">
										  	<div class="row">
    											<div class="col md-8 iconborder">
													<i class="fa fa-heart iconitem"></i>											
    											</div>
												<div class="col md-8 iconborder">
													<i class="fa fa-usd iconitem"></i>
    											</div>   																			
  											</div>									
  										</div>
  										
    										<h6 class="txt=align-left txt-long txt-width">Lazada RM100/150 Voucher Code</h6>
    										<p class="font-10 c-black txt-align-left">Shopee(MY)</p>
    										<p class="font-10 txt-align-left">No Rating Info (2 sold)</p>
											<p class="font-10 txt-align-left c-octa">MYR99.50</p>
  										
  									</div>  									
								</div>
							</div>
                    	</div>    
						</br></br>
                    	<div class="swiper-pagination"></div>
  					</div>	
				</div>
		</div>
        
		
		
		<div class="featurestore ">
			<div class="card">
  				<div class="card-header">
				  	<div class="">
				  		<h5 class="font-30 c-black display-inline">Featured Stores</h5>
						<button class="right bg-octa c-white noneborder font-20" type="submit">View All</button>
					</div> 
				</div> 
  			</div>
 				<div class="card-body">
				  	<div class="swiper-container">
                    	<div class="swiper-wrapper">
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/zalora.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Zolora(MY)</h5>
    									<p class="card-text c-octa">Up to 2% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/Nike.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Nike(MY)</h5>
										<p class="card-text c-octa ">4% Upsized</p>
    									<p class="card-text font-10">Up to 3% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/motherhood.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Motherhood(MY)</h5>
										<p class="card-text c-octa ">6% Upsized</p>
    									<p class="card-text font-10">Up to 4% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/cotton.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Cotton On(MY)</h5>
										<p class="card-text c-octa ">3% Upsized</p>
    									<p class="card-text font-10">Up to 2% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/althea.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Althea</h5>
										<p class="card-text c-octa ">5% Upsized</p>
    									<p class="card-text font-10">Up to 3% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/watson.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Watson(MY)</h5>
										<p class="card-text c-octa ">2% Upsized</p>
    									<p class="card-text font-10">Up to 1% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/flower.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Flower Adviser(MY)</h5>
										<p class="card-text c-octa ">8% Upsized</p>
    									<p class="card-text font-10">Up to 5% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/hermo.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Hermo(MY)</h5>
										<p class="card-text c-octa ">1.5% Upsized</p>
    									<p class="card-text font-10">Up to 1% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
							<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/berry.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Berrylook</h5>
										<p class="card-text c-octa ">10% Upsized</p>
    									<p class="card-text font-10">Up to 8% cashback</p>
  									</div>  									
								</div>
							</div>
                        	<div class="swiper-slide">
								<div class="card" style="width: 18rem;">
									<a href="/">
										<div class="row">
    										<div class="col-1 md-8 ">
											<img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-star"> 
    										</div>
    										<div class="col-6 ">
											<img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="popular-label">
    										</div>										
  										</div>									
										<img src="img/qoo10.png" class="card-img-top " alt="...">
									</a>
  									<div class="card-body">
    									<h5 class="card-title storename">Qoo10(MY)</h5>
    									<p class="card-text c-octa">Up to 1% cashback</p>
  									</div>  									
								</div>
							</div>
                    	</div>    
						</br></br>
            			<div class="swiper-pagination"></div>
            	 	</div>
  				</div>	
		</div>


		<!-- overflow image problem-->
        <div class="event ">
			<div class="card">
  				<div class="card-header">
				  	<div class="">
				  		<h5 class="font-30 c-black display-inline">Event Hightlight(Promo)</h5>
						<button class="right bg-octa c-white noneborder font-20" type="submit">View All</button>
					</div> 
				</div> 
  			</div>
 			<div class="card-body">
			 	<div class="event-container">
                    	<div class="swiper-wrapper">
                        	<div class="event-slide">
								<img src="/img/e1.png" class="eventpng">
							</div>
                        	<div class="event-slide">
							<img src="/img/e2.png" class="eventpng">
							</div>
                        	<div class="event-slide">
							<img src="/img/e3.png" class="eventpng">
							</div>
                        	<div class="event-slide">
							<img src="/img/e4.png" class="eventpng">
							</div>
                        	
                    	</div>    
						</br></br>
                    	<div class="swiper-pagination"></div>
  					</div>	
			</div>	 
				 	
		</div>

  
        <div class="aboutocta">
       		<div class="card-body text-justify">
            	<div>
            		<p class="h5">What is Octaplus</p>
       				 <p>
	        			Octaplus is a shopping guide app that comes with loads of benefits for the users, you can get cashback for purchases, find some of the best ongoing deals on the web and also take a look at reviews of products and services by our users, and you can also redeem great prizes! 
        			</p>
        			<p>
	       				 Octaplus is also an e-commerce cashback platform for the benefit of retailers and consumers. Creating personalized shopping experiences with exclusive promotions and e-community features for consumers combined with high-level targeting and engagement for brands.We reach for the ultimate goal in retail – a shared experience, a recommendation to act on a like or a dislike. OctaPlus has been developed with both consumer and e–retailer in mind, bringing cash back retail shopping and personalized experiences to the consumer and invaluable, interpretive smart data to the retailer or merchant as well as increased traffic.
        			</p>
					<p>
					Get access to a whole range of exclusive offers. Our mission is to make shopping a better experience. Our vision is to be able to meet all your shopping needs. Be rewarded for being part of the Octaplus family through our Rewards. Share and discover the best ways to shop in our growing community in the Review.
					</p>
					<p class="h5">
					What Is Cashback?
					</p>
					<p class="mb-1">
					Earning Cashback as simple as 1-2-3
					</p>
					<p> 
					Sign up to Octaplus for FREE then browse &amp; activities cash back from your desired store. You will be redirected to the merchant site, just shop and pay like you usually do. Your cashback will automatically be credited to your Octaplus account up to 48 hours and you will be notified via email. The amount paid back (cashback) is a percentage of the amount the customer spent on an item. The cashback percentage varies from retailer to retailer, with some businesses offering higher cashback rates than others.
					</p>
					<p class="h5">
					How to Get and Withdraw Your Cashback
					</p>
					<ol>
					<li>
					<p> Log in to your Octaplus account. </p>
					</li>
					<li>
					<p> Look for your favorite online store on Octaplus and click on the store that you’d like to visit. A pop-up will appear and you will be re-directed to the store. Please do not close the tab or your cashback might not be tracked. </p>
					</li>
					<li>
					<p> Shop and complete your order within the same tab so that Octaplus can track your purchase and the amount that you had spent. </p>
					</li>
					<li>
					<p> After you complete a transaction and would like to make a new order, you will need to click through Octaplus again. </p>
					</li>
					<li>
					<p> Repeat this entire process starting at Octaplus every time you make a purchase or you can check out the cashback tips in each store's description at the page bottom. </p>
					</li>
					</ol>
					<p>
					After you’ve accumulated a substantial amount of cashback in your Octaplus account, you may transfer the sum via Transfer to Bank and then withdraw as cash. Do note that the minimum available balance in your account must be RM10. Your withdrawal request will then be processed and deposited into the selected account between 7 and 14 working days.
					</p>
					<p class="h5 font-weight-bold">
					Our Popular Stores
					</p>
					<p class="h5">
					Zalora
					</p>
					<p>
					ZALORA is the leading name in online fashion shopping, carrying an ever-expanding line of local and international brands tailored for consumers in the region. Our selection of over 50.000 products covers every aspect of fashion, from skirts to suits, sneakers to slip-ons, sportswear to watches, and so much more.Start your style journey by owning a well-rounded range of basics and off-duty essentials. 
					</p>
					<p>
					ZALORA shoppers can also shop according to the latest trends that are dominating the fashion runway, whether it’s a monochrome edit, athleisure styling, or this season’s highlights.
					</p>
					<p>
					With the widest selection of fashion apparel, you can possibly find, and outstanding services like lightning-fast shipping, cash-on-delivery, and free returns, ZALORA brings you the best of Malaysia online shopping today!
					</p>
					<p class="h5">
					Cotton On
					</p>
					<p>
					Cotton On Group is Australia's largest global retailer, known for its fashion clothing and stationery brands. It has over 1,500 stores in 18 countries and employs 22,000 workers globally. It currently operates eight brands; Cotton On, Cotton On Body, Cotton On Kids, Rubi, Typo, Cotton On LOST, Factorie and Supré.
					</p>
					<p class="h5">
					Nike
					</p>
					<p>
					NIKE, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear, apparel, equipment, accessories, and services. 
					</p>
					<p class="h5">
					Charles and Keith
					</p>
					<p>
					CHARLES &amp; KEITH started with a vision of creating a line of innovative footwear with a clear design aesthetic for the sensible chic women. Prompted by the pursuit to be directional and innovative in the global market, the brand works closely with appointed business partners to develop at a sharp pace.
					</p>
					<p class="h5">
					Adidas
					</p>
					<p>
					Adidas designs for and with athletes of all kinds. Creators, who love to change the game. Challenge conventions. Break the rules and define new ones. Then break them again. We supply teams and individuals with athletic clothing pre-match. To stay focussed. We design sports apparel that get you to the finish line. To win the match.
					</p>
					<p class="h5">
					Sephora
					</p>
					<p>
					Sephora is a visionary beauty e-retailer in the region, run by a dedicated and experienced affiliate program team. Affiliate marketing has proven to be a key driver, due to Sephora's ever-increasing amount of classic and emerging brands across a broad range of product categories including skincare, color, fragrance, body, smile care, and haircare, in addition to Sephora's own private label.
					</p>
					<p class="h5">
					Poplook
					</p>
					<p>
					POPLOOK.com is the leading online shopping destination in Malaysia. Offering you the widest choice in baju kurung, muslimah dresses, maxi Dresses, jubah, tudung, handbags, jewellery. Free delivery for Malaysia and other selected destinations
					</p>
					<p>
					Modesty, choice, quality, affordability, inclusion and social awareness are the values that underpin the POPLOOK label. With over 1,500 design options; sizes from XS to 4XL; and a seamless online/in-store shopping experience, Malaysia's homegrown Modest Fashion Label hopes to help all women live their best life through fashion. The fashion label carries clothing, headscarves, handbags and shoes as well as a children's range. Each year, the not-for-profit POPLOOK Gives Back campaign channels proceeds to charities that benefit women and children in need. Since its inception in 2009, the label has won numerous accolades, but the most important being the brand of choice to their customers seeking high quality modest fashion.
					</p>
					<p class="h5">
					Pomelo
					</p>
					<p>
					Launched in 2013, Pomelo is a modern fashion brand born in Asia with a global mindset: on-trend, online, on-the-go. With an undisputable sense of style at an unparalleled price, Pomelo aims to offer women everywhere their best look to become their best selves. Shipping is fast, free Min. RM 139 and all products are delivered with a 365 day return policy guaranteed.
					</p>
					<p>
					POMELOFASHION.COM plays host to weekly New Arrivals incorporating basic, core fashion, and trend-led looks.With an ever-growing assortment, Pomelo's style range spans the realms of beachwear to sportswear, and everything in between.From its design studios in Bangkok, to the doorsteps of millions around the world, Pomelo now delivers to over 50 countries globally. Have you tried Pomelo?
					</p>
					<p class="h5">
					Agoda
					</p>
					<p>
					Agoda is one of the world’s fastest growing online travel booking platforms. From its beginnings as an e-commerce start-up based in Singapore in 2005, Agoda has grown to offer a global network of 2 million properties in more than 200 countries and territories worldwide.
					</p>
					<p class="h5">
					Klook
					</p>
					<p>
					Find discounted attraction tickets, tours with unique experiences, and more! Join local day tours to visit spectacular sights and go on delicious food trips around the city. Upon landing at the airport, we've got all kinds of transfers available for you. Discover and book amazing travel experiences with Klook!
					</p>
					<p class="h5">
					KKday
					</p>
					<p>
					KKday provides all kinds of unique experiences. Scuba diving, rock climbing, cooking classes, secret sights, full day tour, tickets, charter service and airport transfers, we’ve got all you want. Arrange your fantastic itinerary for yourself now!
					</p>
					<p class="h5">
					Photobook
					</p>
					<p class="mb-1">
					Photobook Malaysia  offers the best personalisable prints such as canvas prints, cards, stationeries, calendars, prints and photo gifts. We believe your memories should be printed on more than just albums that you can share and admire wherever you are - like a tumbler for the on-the-go and a desk calendar for your office table. Don't forget to keep your home looking good with custom wall decor like metal prints and photo tiles, adding a contemporary style at the same time. Make your own gifts for any occasion including wedding, travel, baby milestone, birthday, graduation, special celebration such as Christmas and Valentine's Day. Curate your best photographs and create a custom gift today through a simple online designer that is programmed with useful tools for every personalisation need.
					</p>
					<p>
					No more worries about finding the perfect gifts because nothing is more unique than personalised gifts from the heart.
					</p>
					<p class="h5">
					Jeoel Jewellery
					</p>
					<p class="mb-1">
					JEOEL is more than jewelry. It is not someone's dream to create just another business. It is not about making fast profits. It is about creating an attainable lifestyle.
					</p>
					<p>
					Joy can be for everyone.
					</p>
					<p class="mb-1">
					The young lady in her first job.The prudent entrepreneur.The hardworking wife and mother.The blushing young man looking for the perfect expression of love.
					</p>
					<p>
					JEOEL looks like fine jewelry without the fine jewelry price tag. We believe everyone deserves to enjoy small luxuries, not just the privileged.
					</p>
					<p class="h5">
					紫藤 Purple Cane
					</p>
					<p>
					Purple Cane Holdings Sdn.Bhd. (298681 V) established in 1987, with innovative spirit integrated with ambitious and enterprising individuals, aiming at going through a cultural passage. According to Purple Cane founder Mr. Lim Hock Lam, the definition of cultural business establishment: is a human activity of the integration of soul, mind and practices. In a culture scene, it is a sublimation of cultural resources, promoting its productivity from low to high through one or more virtual processes or innovation that consequently and incessantly contribute to systematic and value cultural development in the human community. A cultural business establishment entrepreneur or organization are individual or institution involved in cultural activity as obligation and lifestyle.
					</p>
					<p class="h5">
					Focus Point Online Store
					</p>
					<p>
					Focus Point online store offers prescription glasses online at discount prices. Buy quality eyeglasses and contact lenses with us today.
					</p>
					<p>
					Focus Point is officially recognized by the Malaysia Book of Records as the largest optical retail chain store in Malaysia and also the first and only optical retail chain store to be listed in Bursa Malaysia. With more than 180 outlets nationwide and more than 230 eye care professionals ready to serve you. 
					</p>
					<p>
					Customers have a wide range of fashionable eyewear to choose from at the concept stores such as Focus Point, Focus Point Signature, Focus Point Concept Store, Focus Point Outlet, Whoosh, Opulence, Eyefont, Solariz and i-Focus.
					</p>
					<p class="h5">
					Youbeli
					</p>
					<p>
					Youbeli.com is a premier multi-category online marketplace in Malaysia. Fully owned by Youbuy Online Sdn Bhd, Youbeli provides customers with an incredible shopping experience, providing everything at their fingertips, essentially being a one stop shopping destination.
					</p>
					<p>
					<span class="h5 font-weight-normal"> Airasia fresh: </span>
					<span> groceries for everyone </span>
					</p>
					<p>
					Airasia fresh is a new eCommerce platform specialising in Fresh Produce, Frozen Food and Packaged Food. This unique marketplace is powered by Teleport, the logistics arm of airasia. Think of us as your neighbourhood grocery store, with best and freshest produce and food delivered to you the next day! We strive to provide our customers the best value and experience, including secure payment options and the best customer support.
					</p>
					<p class="h5">
					JD Sport
					</p>
					<p>
					JD Sports has everything you need to elevate your everyday casual look to eye-catching new heights. Shop the latest footwear from brands like Nike, adidas, Vans and Puma that deliver performance, style and comfort to fit your on-the-go lifestyle. If you’re looking for athletic slides, basketball sneakers, casual shoes, running gear and everything in between, JD Sports has you covered.
					</p>
					<p class="h5">
					Watsons
					</p>
					<p>
					One of the most well-known pharmaceutical companies in the country, Watson's is the one-stop destination for all your health and beauty products. Watsons Malaysia has a great selection of products for all your health and beauty needs.
					</p>
					<p class="h5">
					Sport Directs
					</p>
					<p>
					Your one stop sport shop for the biggest brands - browse trainers for Men, Women &amp; Kids. Plus sports fashion, clothing &amp; accessories. SportsDirect.com is a British sporting goods retailer, the primary retail asset of Sports Direct International plc. The company was formerly known as Sports Soccer and Sports World, but since 2007 branches of the chain have been re-branded as SportsDirect.com, the domain name of its online presence. 
					</p>
			</div>
			</div>
    	</div>
	</div>	
	@endsection


@section('slideshowscript')
	<script>
		var slideIndex = 0;
			showSlides();

		function showSlides() {
  			var i;
  			var slides = document.getElementsByClassName("mySlides");
  			var dots = document.getElementsByClassName("dot");
  			for (i = 0; i < slides.length; i++) {
    			slides[i].style.display = "none";  
  			}
  			slideIndex++;
  			if (slideIndex > slides.length) {slideIndex = 1}    
  			for (i = 0; i < dots.length; i++) {
    			dots[i].className = dots[i].className.replace(" active", "");
  			}
  			slides[slideIndex-1].style.display = "block";  
  			dots[slideIndex-1].className += " active";
  			setTimeout(showSlides, 3500); // Change image every 2 seconds
			}

			
  </script>
@endsection
