@extends('layouts.app')

@section('content')
    <div class="wrapper create-pizza">
        <h1>Create a new Pizza</h1>
        <form action="/pizzas" method="POST">
            @csrf
            <label for="name">Your Name: </label>
            <input type="text" id="name" name="name">
            </br>
            <label for="type">Choose your pizza type </label>
            <select name ="type" id ="type">
                <option value ="margarita">Margarita</option>
                <option value="hawaiian">Hawaiian</option>
                <option value="veg_supreme">Veg Supreme</option>
                <option value="volcano">Volcano</option>
            </select>
            </br>
            <label for="base">Choose your base type </label>
            <select name ="base" id ="base">
                <option value ="cheesy_crust">Cheesy Crust</option>
                <option value="garlic_crust">Garlic Crust</option>
                <option value="thin&crispy">Thin and Crispy</option>
                <option value="thick">Thick</option>
            </select>
            </br>
            <fieldset>
                <label>Extra Toppings</label></br>
                <input type="checkbox" name="toppings[]" value="mushrooms">Mushrooms</br>
                <input type="checkbox" name="toppings[]" value="peppers">Peppers</br>
                <input type="checkbox" name="toppings[]" value="garlic">Garlic</br>
                <input type="checkbox" name="toppings[]" value="olive">Olive</br>
                <input type="checkbox" name="toppings[]" value="cheese">Cheess</br>
            </fieldset>
            <input type="submit" value ="Order Pizza">
        </form>
    </div>
    

 @endsection