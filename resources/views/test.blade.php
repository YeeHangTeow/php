<div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
    <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
        <a href="https://octaplus.io/store/nike-my/5d504ed53aae55288e11cb33/detail">
            <img src="/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
            <img src="/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
            <img class="img-responsive w-100 mt-4 pt-2" 
                  data-src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" 
                  src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" lazy="loaded">
          </a>
    </div> 
    <p class="text-muted text-truncate my-2 ">
        <a href="https://octaplus.io/store/nike-my/5d504ed53aae55288e11cb33/detail" title="Nike (MY)">
            <p class="mb-0"> Nike (MY) </p>
        </a>
    </p> 
    <div class="h-60 px-2">
        <p class="h6 mb-2 text-truncate text-orange">4% Upsized</p> 
        <p class="text-truncate text-muted mb-0">Up to 3% Cashback</p>
    </div>
</div>