<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>OctaPlus</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
        crossorigin="anonymous">
    <link rel="stylesheet" 
        href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" 
        crossorigin="anonymous">
     
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    

    <!-- Styles -->
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
    <style>
  
  </style>
    

</head>
<body>
<div id="app">
    
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm header borderline ">
            <div class="container ">
                <a class="navbar-brand logowidth" href="{{ url('/') }}">
                   <img src="img/logo.png" class="logo">
                   
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto middlenav ">
                        <li class="nav-item ">
                            <a href ="/" class="nav-item nav-link ">
                            <img src ="img/cashback.png" title = "cashback" class="navlogo ">
                            <span class="nav-link text-dark navspan">Cashback</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a href ="/" class="nav-item nav-link">
                            <img src ="img/deals.png" title = "Deals" class="navlogo">
                            <span class="nav-link text-dark navspan">Deals</span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a href ="/" class="nav-item nav-link">
                            <img src ="img/review.png" title = "Review" class="navlogo">
                            <span class="nav-link text-dark navspan">Review</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                        <a class="nav-link " data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-ellipsis-h"></i>
                        </a>                    
                        <div class="dropdown-menu">
                            <a class="dropdown-item py-2 bg-white text-dark" href="/voucher">
                            <img src ="https://octaplus.io/img/icon/voucher-black.svg" title = "Voucher">
                            <span class="nav-link text-dark navspan">Voucher</span>
                            </a>
                            <a class="dropdown-item py-2 bg-white text-dark" href="/rewards">
                            <img src ="https://octaplus.io/img/icon/reward_black.svg" title = "Rewards" >
                            <span class="nav-link text-dark navspan">Rewards</span>
                            </a>
                            <a class="dropdown-item py-2 bg-white text-dark" href="/wishtree">
                            <img src ="https://octaplus.io/img/icon/wish_black.svg" title = "WishTree">
                            <span class="nav-link text-dark navspan">Wishtree</span>
                            </a>
                        </div>
                        </li>
                    </ul>

                    

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto rightnav">
                        <!-- Authentication Links -->
                        <!-- nav bar right side login register-->
                            
                        @guest
                             <li class="nav-item w3-large">
                                <a href="/faq" class="nav-link text-dark" title="Help">
                                    <i class="fa fa-question-circle  fa-lg"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item nav-link seperator">/</li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
     


       
           
            
</div>
    <main class="py-4">
            @yield('content')
           

            
        </main>

    
    


    <div class="footer">
        <div class="">
        <div class="container">
        <div class="row">
            <div class="col-sm foot">
                <div class=" foot">
                    <img src="img/logo2.png" class="footlogo">
                </div>
            </div>
            <div class="col-sm foot">
                <ul>
                    <li class="list-inline">
                    <p class="footertitle">ABOUT OCTAPLUS</p>
                        
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">About us</a>
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">Term of Service</a>
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">Disclaimer</a>
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">Privacy Policy</a>
                    </li>
                    <li class="list-inline">
                        <a href="/" class="text-dark text-decoration-none">Cashback Terms</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm foot">
                <ul>
                    <li class="list-inline">
                    <p class="footertitle">MORE FROM US</p>
                        
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">FAQ</a>
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">Events</a>
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">Blog</a>
                    </li>
                    <li class="list-inline">
                        <a href="/"  class="text-dark text-decoration-none">Customer Review</a>
                    </li>
                    <li class="list-inline">
                        <a href="/" class="text-dark text-decoration-none">Career</a>
                    </li>
                </ul>
            </div>
            <div class="">
            <ul>
                    <li class="list-inline">
                    <p class="footertitle">MOBILE APP DOWNLOAD</p>
                        
                    </li>
                    <li class="list-inline">
                        <div class="display-inline">
                            <a href="/" class="" download">
                                <img src="/img/appledownload.png">                               
                            </a>
                            <a href="/" class=" download">
                                <img src="/img/googleplaydownload.jpg">                               
                            </a>
                        </div>
                    </li>
                    
                    <li class="list-inline">
                    <p class="footertitle">FOLLOW US</p>
                        
                    </li>
                    <li class="list-inline">
                    <div class="display-inline">
                            <a href="/" class="">
                                <img src="/img/facebook.png" class="download-icon">                               
                            </a>
                            <a href="/" class="">
                                <img src="/img/twitter.jpg" class="download-icon">                               
                            </a>
                            <a href="/" class="">
                                <img src="/img/instagram.jpg" class="download-icon">                               
                            </a>
                            <a href="/" class="">
                                <img src="/img/youtube.png" class="download-icon">                               
                            </a>
                            <a href="/" class="">
                                <img src="/img/pinterest.png" class="download-icon">                               
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
       
        </div>
        <div class="copyright">
        © 2019 Octaplus - Buy . Earn . Explore | All Rights Reserved.
        </div>    
    </div>
       

    
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 4,
      spaceBetween: 15,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });

    var swiper2 = new Swiper('.event-container', {
      slidesPerView: 10,
      spaceBetween: 15,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        
      },
    });
  

   
  </script>  
  @yield ('slideshowscript')
</body>
</html>
